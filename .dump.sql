DROP TABLE IF EXISTS `tokens`;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(48) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `email` varchar(64) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `instagram` varchar(32) NOT NULL,
  `num` tinyint(3) unsigned NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `range` varchar(8) NOT NULL,
  `desc` text NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `categories` (`id`, `name`, `range`, `desc`, `num`) VALUES
(1,	'Bucin Biasa',	'60-100',	'Kamu masih dalam tahap level bucin biasa. Kadang kamu jadi bucin, kadang kamu masih menggunakan logika. Ini nih lirik yang cocok buat menggambarkan level kebucinan kamu:\r\nI don\'t really give a damn about the way you touch me\r\nWhen we\'re alone\r\nYou can hold my hand',	0),
(2,	'Bucin Gila',	'101-130',	'Memang sih kalau punya gebetan atau pacar rasanya pengen ngapa-ngapain berdua. Tapi hati-hati kayaknya kamu udah mulai ketergantungan sama dia, walaupun ga sering-sering amat sih. Mungkin lirik ini yang bisa gambarin posisi kamu sekarang:\r\nDo you like it when I\'m away?\r\nIf I went and hurt my body, baby\r\nWould you love me the same?',	0),
(3,	'Bucin Akut',	'131-180',	'Hmm.. Kayaknya perasaan cinta kamu ke dia udah harus diwaspadai lho, karena sangking besarnya cinta kamu sama dia, sampai membuat kamu jadi buta dan bahkan cenderung menyakiti diri kamu sendiri, dia, dan juga orang disekitar kalian. Ingat, kamu dan dia sama-sama butuh waktu me time lho. Mungkin lirik ini, ngegambarin posisi kamu sekarang:\r\nBaby, I am a wreck when I\'m without you\r\nI need you here to stay\r\nI broke all my bones that day I found you',	0);


DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `answers_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;