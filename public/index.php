<?php
header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
echo "<!--SSR-->\n";

$files = scandir('./', 1);

$target = '';

for ($i = 0; $i < count($files); $i++) {
    $f = $files[$i];
    $f_arr = explode('.', $f);
    if (count($f_arr >= 1) && $f_arr[1] == 'html') {
        $target = $f;
        break;
    }
}

readfile('./' . $target);
