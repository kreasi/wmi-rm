<?php
/*
 * [GET] /api/index.php/get_sharer
 */
$actual_link =
    (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'
        ? 'https'
        : 'http') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$actual_link_arr = explode('/', $actual_link);

$new_link_arr = [];

for ($i = 0; $i < count($actual_link_arr); $i++) {
    if ($actual_link_arr[$i] == 'api') {
        break;
    }

    array_push($new_link_arr, $actual_link_arr[$i]);
}

$new_link = implode('/', $new_link_arr) . '/';

$text =
    'Ternyata cintaku kepadanya sudah membuatku menjadi __BUCIN__! Kalau kamu gimana? Yuk, ikutan personality quiz Ricky Montgomery dan menangkan uang tunai sebesar Rp3.000.000 untuk 3 orang pemenang! ' .
    $new_link;

echo json_encode([
    'sharer' => [
        'base_url' => $new_link,
        'facebook' =>
            'https://www.facebook.com/sharer/sharer.php?u=' .
            urlencode($new_link),
        'twitter' =>
            'https://twitter.com/intent/tweet?text=' . urlencode($text),
    ],
]);
