<?php
header('Content-type:application/json;charset=utf-8');

include_once './_connection.php';

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

if ($uri) {
    $uri_arr = explode('/', $uri);
    $basename = end($uri_arr);

    if ($basename && file_exists('./' . $basename . '.php')) {
        include_once './' . $basename . '.php';
    } else {
        include_once './404.php';
    }
}

$db->close();
