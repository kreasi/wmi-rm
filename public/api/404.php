<?php
/*
 * [GET] /api/index.php/*
 */
echo json_encode([
    'success' => false,
    'message' => "This route doesn't exist.",
]);
