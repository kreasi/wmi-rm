<?php
/*
 * [POST] /api/index.php/post_answer
 */
$data = json_decode(file_get_contents('php://input'), true);

if (
    $data &&
    $data['token'] &&
    // $data['name'] &&
    $data['email'] &&
    $data['phone'] &&
    $data['instagram'] &&
    $data['answers'] &&
    $data['result']
) {
    $get_token = $db
        ->query(
            'SELECT `id` FROM `tokens` WHERE `token` = ? LIMIT 1',
            $data['token']
        )
        ->fetchArray();

    if ($get_token && $get_token['id']) {
        $user_id = 0;

        $get_user = $db
            ->query(
                'SELECT `id`, `num` FROM `users` WHERE `email` = ? AND `instagram` = ? LIMIT 1',
                $data['email'],
                $data['instagram']
            )
            ->fetchArray();

        if ($get_user && $get_user['id']) {
            $user_id = $get_user['id'];
            $play_num = $get_user['num'] + 1;

            $db->query(
                'UPDATE `users` SET `num` = ? WHERE `id` = ?',
                $play_num,
                $user_id
            );
        } else {
            $db->query(
                'INSERT INTO `users` (`email`, `phone`, `instagram`, `num`, `created_at`) VALUES (?, ?, ?, ?, ?)',
                $data['email'],
                $data['phone'],
                $data['instagram'],
                1,
                date('Y-m-d H:i:s')
            );

            $user_id = $db->lastInsertID();
        }

        $get_category = $db
            ->query(
                'SELECT `id`, `num` FROM `categories` WHERE `id` = ?',
                $data['result']
            )
            ->fetchArray();

        if ($get_category['id']) {
            $db->query(
                'INSERT INTO `answers` (`user_id`, `category_id`, `answer`, `created_at`) VALUES (?, ?, ?, ?)',
                $user_id,
                $get_category['id'],
                json_encode($data['answers']),
                date('Y-m-d H:i:s')
            );

            $cat_num = $get_category['num'] + 1;

            $db->query(
                'UPDATE `categories` SET `num` = ? WHERE `id` = ?',
                $cat_num,
                $get_category['id']
            );

            echo json_encode([
                'success' => true,
                'message' => 'Data saved.',
            ]);
        } else {
            echo json_encode([
                'success' => false,
                'message' => 'Invalid result.',
            ]);
        }
    } else {
        echo json_encode([
            'success' => false,
            'message' => 'Invalid token.',
        ]);
    }
} else {
    echo json_encode([
        'success' => false,
        'message' => 'Incomplete data.',
    ]);
}
