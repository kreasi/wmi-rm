export default {
  BASE_API: 'api/index.php/',
  // BASE_API: 'http://localhost:5001/api/index.php/',
  API_TOKEN: 'wmi2021',
  APP_NAME: 'Ricky Montgomery',
  APP_TITLE:
    "&ldquo;<em>She's a, she's a lady, and I am just a line without a hook </em>&rdquo;",
  APP_DESC:
    'Seperti lirik di atas, yuk, cari tahu seberapa bucinkah kamu sama pasangan kamu? Tapi sebelum kita mulai, isi data diri kamu di bawah ini, ya.',
  QUESTIONS: [
    {
      label:
        'Dalam sehari, seberapa seringkah kamu harus ngabarin pasangan/gebetan kamu?',
      type: 'text',
      options: [
        {
          label: 'Beberapa 3-4 jam sekali, yang penting ga ngilang.',
          score: 20
        },
        {
          label:
            'Pas pagi, siang, atau malem doang. Dia ngerti, kok, gw sibuk.',
          score: 10
        },
        {
          label: 'Setiap saat gw harus komunikasi terus sama dia.',
          score: 30
        }
      ]
    },
    {
      label:
        'Apakah kamu pernah berbohong sama sahabat, teman, atau keluarga kamu demi jalan sama pacar/gebetan kamu?',
      type: 'text',
      options: [
        {
          label: 'Ga pernah sama sekali, dong.',
          score: 10
        },
        {
          label: 'Hmmm.. Kayaknya sering banget, deh.',
          score: 30
        },
        {
          label: 'Cuman 2-4 kali, lah.',
          score: 20
        }
      ]
    },
    {
      label:
        'Apakah kamu pernah jadi ga mood seharian atau males ngapa-ngapain karena kamu berantem sama dia?',
      type: 'text',
      options: [
        {
          label: 'Sering banget, dia tuh kayak bisa ngendaliin mood gw banget.',
          score: 30
        },
        {
          label: 'Ga pernah, dong, gw bisa jaga mood gw.',
          score: 10
        },
        {
          label: 'Kadang, sih, kalau lagi ribut gede.',
          score: 20
        }
      ]
    },
    {
      label:
        'Kalau kamu berantem sama doi, siapa, sih, yang ngehubungin dan minta maaf duluan?',
      type: 'text',
      options: [
        {
          label: 'Gw lah, mana mau dia hubungin gw duluan.',
          score: 30
        },
        {
          label: 'Dia, dong.',
          score: 10
        },
        {
          label: 'Kadang dia, kadang gw, tergantung yang salah siapa.',
          score: 20
        }
      ]
    },
    {
      label:
        'Siapa, sih, yang lebih sering inisiatif buat nelpon/video call/nge-date duluan?',
      type: 'text',
      options: [
        {
          label: 'Sama-sama inisiatif, kok.',
          score: 20
        },
        {
          label: 'Gw, sih, yang lebih sering ngajakin duluan.',
          score: 30
        },
        {
          label: 'Dia, sih, yang lebih sering inisiatif.',
          score: 10
        }
      ]
    },

    {
      label:
        'Ketika dia sudah melakukan sesuatu yang sangat buruk sehingga kamu merasa sakit hati &amp; kecewa, apa lagi karena dia sudah melakukan hal ini untuk kesekian kalinya, apa yang akan kamu lakukan?',
      type: 'text',
      options: [
        {
          label: 'Udahin hubungan ini &amp; move on.',
          score: 10
        },
        {
          label: 'Maafin dia, cinta harus memaafkan, bukan?',
          score: 30
        },
        {
          label:
            'Kasih kesempetan terakhir, tapi kalau masih sama, kamu akan memutuskan untuk beranjak dari hubungan ini.',
          score: 20
        }
      ]
    }
  ]
}
